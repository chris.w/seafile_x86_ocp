#!/bin/bash

## prepare env for seafile user
USERNAME=seafile
SEAFDIR="/home/seafile"
BUILDVERSION=$(cat /home/seafile/.build)
SEAFWORKDIR=$SEAFDIR"/seafile-server-latest"

echo -e "running as USER:" $(whoami)

# wait until database server is up and running
sleep 1

cd ${SEAFDIR}

startSeafile(){
# debug option

    if [ ${SEAFILE_DEBUG} -eq "1" ] ; then
        "${SEAFWORKDIR}"/seafile.sh start && "${SEAFWORKDIR}"/seahub.sh start-fastcgi
        tail -f /dev/null
    else
		#start server
		"${SEAFWORKDIR}"/seafile.sh start && "${SEAFWORKDIR}"/seahub.sh start
		tail -f /dev/null
	fi	
}

# check if user is seafile
if [ "$(whoami)" != "$USERNAME" ]; then
        echo "Script must be run as user: seafile"
        exit 255
        if [ ${SEAFILE_DEBUG} -eq 1 ] ; then
            tail -f /dev/null
        fi
else
# check if external volumes are mounted. the following foldes must exist in /opt
	dirs=(
	    conf
	    ccnet
	    seafile-data
	    seahub-data
	    pro-data
	    seafile-license.txt
	    logs
	)

	# check if .seafile_Version exist. if not, the server must be set up from scratch by running initSeafile.sh
	if [ -e /opt/seafile/.seafile_version ]; then 
        RUNNINGVERSION=$(cat /opt/seafile/.seafile_version)
        
		# relink folders from storage path to home
		for d in ${dirs[*]}; do
    		src=/opt/seafile/$d
		if [[ -e $src ]]; then
                echo "remove /home/seafile/$d and link from $src to /home/seafile/"
        		rm -rf /home/seafile/$d && ln -sf $src /home/seafile/
	    	fi
		done
	
		ln -s /home/seafile/seafile-server-${BUILDVERSION} $SEAFDIR"/seafile-server-latest"
	
		# check if seafile needs to be updated
		echo -e "BUILD Version = $BUILDVERSION"
		echo -e "Running Version = $RUNNINGVERSION"

		if [ $BUILDVERSION != $RUNNINGVERSION ] ; then
		
		# RUN UPGRADE
		    echo -e "current version and docker built version differ. running upgrade from $RUNNINGVERSION  to $BUILDVERSION ..."
	    
		    echo -e "remove link to seafile directory..."
		    rm  ${SEAFWORKDIR}
    	
		    echo -e "executing ${SEAFDIR}/seafile-server-${BUILDVERSION}/upgrade/$SEAFILE_UPGRADESCRIPT"
		    ${SEAFDIR}/seafile-server-${BUILDVERSION}/upgrade/$SEAFILE_UPGRADESCRIPT
    	
		   "${SEAFWORKDIR}"/seafile.sh stop && "${SEAFWORKDIR}"/seahub.sh stop
		fi
		startSeafile
	else
		# init seafile
        	echo -e "no .seafile_version found. Seafile has probably not been initialized."
	        /opt/initSeafile.sh $SEAFDIR/seafile-server-${BUILDVERSION}
        	echo "Seafile setup complete. Now moving initialized folders to persistant storage"
            
	        for d in ${dirs[*]}; do
                src=/home/seafile/$d
                if [[ -e $src ]]; then
                    echo "move /home/seafile/$d to /opt/seafile/"
                    mv $src /opt/seafile/
                fi
            done
            for d in ${dirs[*]}; do
                src=/opt/seafile/$d
                if [[ -e $src ]]; then
                    echo "link from $src to /home/seafile/"
                    ln -s $src /home/seafile/
                fi
            done

        echo "try to stop running service..."
        "$SEAFDIR/seafile-server-${BUILDVERSION}"/seafile.sh stop && "$SEAFDIR/seafile-server-${BUILDVERSION}"/seahub.sh stop
        pkill python3
        startSeafile
	fi
fi
