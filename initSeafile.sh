#!/bin/bash
SEAFDIR=/home/seafile
SEAFWORKDIR=/home/seafile/seafile-server-latest
BUILDVERSION=$(cat /home/seafile/.build)
SEAFILE_SETUP_DIR=/home/seafile/seafile-server-${BUILDVERSION}


backupConfig(){

    SEAHUB_CONF=${SEAFDIR}/conf

    if [[ -d "${SEAHUB_CONF}" ]] ;then
        echo "${SEAHUB_CONF} exists on your filesystem. Backing up to ${SEAHUB_CONF}_bak"
        mv "${SEAHUB_CONF}" "${SEAHUB_CONF}_bak"
    fi
}

#usage: setup-seafile-mysql.py [-h] [-n SERVER_NAME] [-i SERVER_IP]
#                              [-p FILESERVER_PORT] [-d SEAFILE_DIR]
#                              [-e USE_EXISTING_DB] [-o MYSQL_HOST]
#                              [-t MYSQL_PORT] [-u MYSQL_USER]
#                              [-w MYSQL_USER_PASSWD] [-q MYSQL_USER_HOST]
#                              [-r MYSQL_ROOT_PASSWD] [-c CCNET_DB]
#                              [-s SEAFILE_DB] [-b SEAHUB_DB]
#   Option              Script parameter 	 Environment variable 	 Default value
#   server name 	                -n 	     SERVER_NAME 	         hostname -s(short host name)
#   server ip or domain             -i 	     SERVER_IP 	             hostname -i(address for the host name)
#   fileserver port 	            -p 	     FILESERVER_PORT 	     8082
#   seafile data dir 	            -d 	     SEAFILE_DIR 	         current directory
#   use existing db 	            -e 	     USE_EXISTING_DB 	     0(create new db)
#   mysql server host 	            -o 	     MYSQL_HOST 	         127.0.0.1
#   mysql server port 	            -t 	     MYSQL_PORT 	         3306
#   mysql root password 	        -r 	     MYSQL_ROOT_PASSWD 	     no default value(must be set when create new db)
#   mysql user for seafile 	        -u 	     MYSQL_USER 	         no default value(must be set)
#   password for seafile mysql user -w 	     MYSQL_USER_PASSWD 	     no default value(must be set)
#   mysql user host 	            -q 	     MYSQL_USER_HOST 	     no default value(must be set when create new db and using non local mysql server)
#   ccnet dabase name 	            -c 	     CCNET_DB 	             ccnet-db
#   seafile dabase name 	        -s 	     SEAFILE_DB 	         seafile-db
#   seahub dabase name 	            -b 	     SEAHUB_DB 	             seahub-db


initSeafile() {
${SEAFILE_SETUP_DIR}/setup-seafile-mysql.sh auto -e ${USE_EXISTING_DB} -d "/home/seafile/seafile-data" -n ${SERVER_NAME} -i ${SERVER_IP} -p ${FILESERVER_PORT} -o ${MYSQL_HOST} -t ${MYSQL_PORT} -r ${MYSQL_ROOT_PASSWD} -u ${MYSQL_USER} -w ${MYSQL_USER_PASSWD} -c ${CCNET_DB} -s ${SEAFILE_DB} -b ${SEAHUB_DB} -q${MYSQL_USER_HOST}
}

configSeafile(){
    echo -e "Seafile Server initialized. Now enabling seafdav" \
    && eval "sed -i 's/enabled = false/enabled = true/g' ${SEAFDIR}/conf/seafdav.conf" \
    && echo -e "make seafile available from outside" \
    && echo -e "Creating admin account... save original adminscript" \
    && cp ${SEAFILE_SETUP_DIR}/check_init_admin.py ${SEAFILE_SETUP_DIR}/check_init_admin.py.bak \
    && echo -e "replace adminuser" \
    && eval "sed -i 's/= ask_admin_email()/= \"${SEAFILE_ADMIN_EMAIL}\"/' ${SEAFILE_SETUP_DIR}/check_init_admin.py" \
    && echo -e "replace generated password" \
    && eval "sed -i 's/= ask_admin_password()/= \"${SEAFILE_ADMIN_PASSWORD}\"/' ${SEAFILE_SETUP_DIR}/check_init_admin.py"

    ${SEAFWORKDIR}/seafile.sh start \
    && ${SEAFWORKDIR}/seahub.sh start \
    && ${SEAFWORKDIR}/seahub.sh stop \
    && ${SEAFWORKDIR}/seafile.sh stop

    mv ${SEAFILE_SETUP_DIR}/check_init_admin.py.bak ${SEAFILE_SETUP_DIR}/check_init_admin.py

    export resetadmin=${SEAFWORKDIR}/reset-admin.sh

    echo -e "\n\nAdmin Login is ${SEAFILE_ADMIN_EMAIL}:${SEAFILE_ADMIN_PASSWORD}\n\n" 
    echo ${SEAFILE_VERSION} > /opt/seafile/.seafile_version
}

fixAfterInit(){

    # add the following lines to seahub-settings.py to prevent wrog url for media contents in combination of dedicated nginx-proxy
    # see https://forum.seafile.com/t/avatar-in-share-dialog-missing/4179/21
    # check first, if parameters are already set and the add this lines

    SEAHUB_CONF=${SEAFDIR}/conf

    echo -e "changing some config files...\n"
    
    echo -e "...set USE_X_FORWARDED_HOST in seahub_settings.py"
    sed --in-place '/USE_X_FORWARDED_HOST/d' $SEAHUB_CONF/seahub_settings.py
    echo "USE_X_FORWARDED_HOST = True" >> $SEAHUB_CONF/seahub_settings.py

    echo -e "...set SECURE_PROXY_SSL_HEADER in seahub_settings.py"
    sed --in-place '/SECURE_PROXY_SSL_HEADER/d' $SEAHUB_CONF/seahub_settings.py
    echo "SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')" >> $SEAHUB_CONF/seahub_settings.py
    
#    echo -e "... set memcache"
#    sed --in-place "
#    CACHES = {
#    'default': {
#        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#        'LOCATION': '127.0.0.1:11211',
#    }" >> $SEAHUB_CONF/seahub_settings.py
#}
    
    echo -e "...set BIND in gunicorn.conf.py"
    sed --in-place 's/127.0.0.1/0.0.0.0/g'  $SEAHUB_CONF/gunicorn.conf.py
}


# run initialization
if [ "$(whoami)" != "seafile" ]; then
        echo "Script must be run as user: seafile"
        exit 255
else
    if [ -d "$SEAFILE_SETUP_DIR" ]; then
    # Take action if DIR exists. #
        echo "Setup from $SEAFILE_SETUP_DIR"
        initSeafile
        configSeafile
        fixAfterInit
        #echo -e "\n Seafile will be started... \n\n"
        #${SEAFWORKDIR}/seafile.sh start \
        #&& ${SEAFWORKDIR}/seahub.sh start
        #unsetEnv
    else
        echo "$SEAFILE_SETUP_DIR not found!"
    fi
fi
