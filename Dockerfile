FROM docker.io/ubuntu:latest
ARG VERSION=9.0.2
SHELL ["/bin/bash", "-c"]
RUN apt update && apt install -y wget
RUN cd /opt \
	&& wget https://raw.githubusercontent.com/haiwen/seafile-server-installer/master/seafile-9.0_ubuntu

EXPOSE 8000
EXPOSE 8080
EXPOSE 8082

WORKDIR /home/seafile
